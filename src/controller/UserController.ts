import {NextFunction, Request, Response} from "express";
import {User} from "~/entity";

export class UserController {
    public static async user(req: Request | any, res: Response, next: NextFunction) {
        return res.status(200).json({
            user: req.account,
        });
    }

    // async one(request: Request, response: Response, next: NextFunction) {
    //     return this.userRepository.findOne(request.params.id);
    // }
    //
    // async save(request: Request, response: Response, next: NextFunction) {
    //     return this.userRepository.save(request.body);
    // }
    //
    // async remove(request: Request, response: Response, next: NextFunction) {
    //     let userToRemove = await this.userRepository.findOne(request.params.id);
    //     await this.userRepository.remove(userToRemove);
    // }

}
