import {Request, Response, NextFunction} from "express";
import {json_data, hashPassword, checkPassword, makeJWT} from '~/utils/functions';
import {User} from '~/entity';

export class AuthController {
    public async login(req: Request, res: Response, next: NextFunction) {
        let {body} = req;
        let {password, username, email} = body;
        let user: User | undefined = await User.findOne({
            where: [
                {username},
                {email: username},
            ],
        });

        if (!user) {
            return res.status(404).json(json_data([], "Пользователь не зарегистрирован"));
        }

        let validatePass = await checkPassword(password, user.password);
        if (validatePass) {
            let response = makeJWT(user.id);
            return res.status(200).json(response);
        }
        return res.status(400).json(json_data([], "Неверный пароль"));
    }

    async register(req: Request, res: Response, next: NextFunction) {
        let {body} = req;
        let {password, password_confirmation, username, email} = body;
        let user = await User.findOne({
            username: body.username,
        });

        if (user) {
            return res.status(400).json(json_data([], "Пользователь уже зарегистрирован"));
        }

        if (password !== password_confirmation) {
            return res.status(400).json(json_data([], "Подтверждение пароля не совпадает с паролем"));
        }

        let hash = await hashPassword(password);
        const newUser = User.create({
            username,
            email,
            password: hash,
        });
        newUser.save().then(user => res.status(200).json(json_data(user, 'Пользователь успешно зарегистрирован')))
            .catch(error => res.status(400).json(json_data(error, 'Ошибка при создании пользователя')));
    }
}
