import { Fileable } from "../entity/Fileable";

export default interface iFileables {
    fileable?: Fileable[],
    id?: number,
}
