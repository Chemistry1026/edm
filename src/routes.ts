import {UserController, AuthController} from "./controller";
import * as passport from 'passport';

let route = [];

const config = {
    session: false,
};

let authRoutes = [
    {
        method: "post",
        route: "/login",
        controller: AuthController,
        action: 'login',
    },
    {
        method: "post",
        route: "/register",
        controller: AuthController,
        action: 'register',
    },
];

let userRoutes = [
    {
        method: "get",
        route: "/user",
        controller: UserController,
        action: "routes.ts.ts",
        middleware: passport.authorize('jwt', config),
    }
];

route = route.concat(authRoutes, userRoutes);
export const Routes = route;
