import {Entity, PrimaryGeneratedColumn, Column, Index, ManyToOne} from "typeorm";
import {validateURL, url, trimUrl} from '../utils/functions';
import {Fileable} from "./Fileable";

@Entity()
export class File {//TODO MAKE relation
    @PrimaryGeneratedColumn()
    id!: number;

    @Column("varchar")
    name?: string;

    @Column("varchar")
    _path?: string;

    @Column("date", {
        default: "now()",
    })
    createdAt?: string;

    @Column({
        type: "date",
        default: "now()",
        onUpdate: "now()",
    })
    updatedAt?: string;

    public get path() {
        let path = this._path;
        if (path) {
            return validateURL(path) ? path : url(path);
        }
    }

    public set path(val: string) {
        this._path = val;
    }

    public get fullPath() {
        return `${trimUrl(this.path)}/${this.name}`;
    }

    @ManyToOne(type => Fileable, fileable => fileable.files)
    fileable!: Fileable[];
}
