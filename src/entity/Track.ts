import {Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToOne, JoinColumn, BaseEntity} from "typeorm";

import {Artist} from "./Artist";
import {Fileable} from "./Fileable";
import {File} from "./File";

import iFileables from "../interfaces/fileables"

@Entity()
export class Track extends BaseEntity implements iFileables {//TODO Format MODEL and MAKE relation

    @PrimaryGeneratedColumn()
    id!: number;

    @Column("timestamp", {
        default: "now()",
    })
    createdAt?: string;

    @Column({
        type: "timestamp",
        default: "now()",
        onUpdate: "now()",
    })
    updatedAt?: string;

    @ManyToMany(type => Artist)
    @JoinTable()
    artists!: Artist[];

    @OneToOne(() => Fileable, {nullable: true})
    @JoinColumn()
    fileable?: Fileable[];
}
