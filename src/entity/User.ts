import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, Index} from "typeorm";
import {Length, IsEmail, MinLength} from "class-validator";

@Entity()
@Index(["username", "email"], {unique: true})
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({
        type: "varchar",
        unique: true,
    })
    @Length(3, 16)
    username!: string;

    @Column({
        type: "varchar",
        unique: true,
    })
    @IsEmail()
    email!: string;

    @Column({
        type: "varchar",
        length: 255,
        select: false,
    })
    @MinLength(6)
    password!: string;

    @Column({
        type: "varchar",
        nullable: true,
    })
    firstName?: string;

    @Column({
        type: "varchar",
        nullable: true,
    })
    lastName?: string;

    @Column({
        type: "smallint",
        nullable: true,
    })
    age?: number;

    @Column({
        type: "text",
        nullable: true,
    })
    description?: string;

    @Column("timestamp", {
        default: "now()",
    })
    createdAt?: string;

    @Column({
        type: "timestamp",
        default: "now()",
        onUpdate: "now()",
    })
    updatedAt?: string;

}
