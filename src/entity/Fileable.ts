import {Entity, Column, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import { File } from "./File";

@Entity()
export class Fileable {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column("int", {
        primary: true,
    })
    fileId!: number;

    @Column("int", {
        primary: true,
    })
    fileableId!: number;

    @Column("varchar", {
        primary: true,
    })
    fileableType!: string;

    @OneToMany(type => File, file => file.fileable)
    files!: File[];
}
