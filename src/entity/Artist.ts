import {Column, Entity, BaseEntity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Artist extends BaseEntity {//TODO Format MODEL and MAKE relation
    @PrimaryGeneratedColumn()
    id!: number;

    @Column("timestamp", {
        default: "now()",
    })
    createdAt?: string;

    @Column({
        type: "timestamp",
        default: "now()",
        onUpdate: "now()",
    })
    updatedAt?: string;
}
