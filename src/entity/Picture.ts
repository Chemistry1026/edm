import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Picture {//TODO Format MODEL and MAKE relation
    @PrimaryGeneratedColumn()
    id!: number;

    @Column("timestamp", {
        default: "now()",
    })
    createdAt?: string;

    @Column({
        type: "timestamp",
        default: "now()",
        onUpdate: "now()",
    })
    updatedAt?: string;
}
