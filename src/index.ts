import "reflect-metadata";
import {createConnection} from "typeorm";
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
// import * as companion from '@uppy/companion';
// import * as tus from 'tus-node-server';
import logger from 'morgan';
import path from 'path';
import dotenv from 'dotenv';
import passport from '~/utils/passport';
dotenv.config();

import routes from '~/routes/index';
import userRoutes from '~/routes/user';

createConnection().then(async connection => {
    // const Server = tus.Server;
    // const EVENTS = tus.EVENTS;
    // const tusClass = new Server();
    // tusClass.on(EVENTS.EVENT_UPLOAD_COMPLETE, event => {
    //     console.log(`Upload complete for file ${event.file.id}`);
    // }).on(EVENTS.EVENT_FILE_CREATED, event => {
    //     console.log(event);
    // }).on(EVENTS.EVENT_ENDPOINT_CREATED, event => {
    //     console.log(event);
    // });

    // const tusServer = new tus.Server();
    //
    // tusServer.datastore = new tus.FileStore({
    //     path: '/files'
    // });

    // create express app
    const app = express();
    // const uploadApp = express();
    // uploadApp.all('*', tusServer.handle.bind(tusServer));
    //
    // const UPPY_PORT: string = process.env.UPPY_PORT || '3020';
    // const PROTOCOL: string = process.env.PROTOCOL || 'http';
    // const SECRET: string = process.env.SECRET_KEY || '6sKwbRIC6lNW0jQv';
    // const DEBUG: boolean = !!(process.env.DEBUG);
    // const options = {
    //     server: {
    //         host: `localhost:${UPPY_PORT}`,
    //         protocol: PROTOCOL,
    //     },
    //     filePath: '/files/',
    //     secret: SECRET,
    //     debug: DEBUG,
    // };

    app.use(logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded({extended: false}));
    app.use(express.static(path.join(__dirname, 'files')));
    app.use(cors());
    // app.use(companion.app(options));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(bodyParser.json());

    // app.use('/uploads', uploadApp);

    //Routes
    app.use('/', routes);
    app.use('/user', userRoutes);

    // setup express app here
    // ...

    // start express server
    const port = process.env.PORT || 3000;
    const server = app.listen(port);
    // companion.socket(server, options);

    // insert new users for test
    // await connection.manager.save(connection.manager.create(User, {
    //     firstName: "Timber",
    //     lastName: "Saw",
    //     age: 27
    // }));
    // await connection.manager.save(connection.manager.create(User, {
    //     firstName: "Phantom",
    //     lastName: "Assassin",
    //     age: 24
    // }));
    console.info(`Server started: http://localhost:${port}`);
}).catch(error => console.log(error));
