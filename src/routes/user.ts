import { Router } from 'express';
import { UserController } from '~/controller';
import passport from '~/utils/passport';

const router = Router();
const userController = new UserController();
const config = {
    session: false,
};

router.use(passport.authorize('jwt', config));
router.get('/', UserController.user);

export default router;
