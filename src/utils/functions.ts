import dotenv from 'dotenv';
import fs from 'fs';
import jwt from 'jsonwebtoken';
import path from 'path';
import bcrypt from 'bcrypt';

dotenv.config();

const functions = {
    url(path: string) {
        return `${process.env.APP_URL}/${path}`;
    },

    validateURL(str: string) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    },

    json_data(data: [] | object = [], message: string = '') {
        return {
            data,
            message,
        }
    },

    hashPassword(password: string) {
        const saltRounds: number = parseInt(process.env.SALT || '10');
        return bcrypt.hash(password, saltRounds);
    },

    checkPassword(password: string, hash: string) {
        return bcrypt.compare(password, hash);
    },

    // async makeFaker(callback, count = 15) {
    //     let items = [];
    //     const faker = require('faker/locale/ru');
    //     for (let i = 0; i < count; i++) {
    //         items.push(await callback(faker));
    //     }
    //
    //     return items;
    // },

    trimUrl(str: string) {
        return str.replace(/\/$/, '');
    },

    makeJWT(id: number) {
        let payload = {id};
        let token = jwt.sign(payload, process.env.JWT_SECRET || 'secret');
        return {
            access_token: token,
            message: 'Вы успешно авторизовались',
        };
    },

    errors_data(errors: [], message = "The given data was invalid.") {
        return {}
    },

    fileExists(file: string) {
        return fs.existsSync(file);
    },

    parsePath(file: string) {
        return path.parse(file);
    },
};

export = functions;
