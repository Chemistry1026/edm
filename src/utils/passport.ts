import passport from 'passport';
import {Strategy, ExtractJwt} from 'passport-jwt';
import dotenv from 'dotenv';
import {User} from '~/entity';

dotenv.config();

let JWTOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET,
};

passport.use(new Strategy(JWTOptions,
    (payload, next) => {
        User.findOne({
            id: payload.id,
        }).then((user) => {
            return next(null, user)
        }).catch(err => {
            return next(null, false);
        });
    }
));

passport.serializeUser(function (user: User, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id: number, cb) {
    User.findOne({
        id
    }).then((user) => {
        if (user) {
            return cb(null, user.id)
        }
    }).catch(err => {
        return cb(err);
    });
});

export default passport;
